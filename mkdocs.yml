nav:
  - Home: index.md
  - Cori GPU nodes:
    - Introduction: cgpu/index.md
    - Hardware Info: cgpu/hardware.md
    - Slurm Access: cgpu/access.md
    - Usage: cgpu/usage.md
    - Software: 
      - Overview: cgpu/software/index.md
      - Compilers and Offloading: cgpu/software/compilers.md
      - Math Libraries: cgpu/software/math.md
      - Deep Learning: cgpu/software/dl.md
      - Python: cgpu/software/python.md
      - Shifter: cgpu/software/shifter.md
      - Profiling: cgpu/software/profiling.md
      - Debugging: cgpu/software/debug.md
      - Known Issues: cgpu/software/issues.md
    - Examples: cgpu/examples.md
    - Help: cgpu/help.md
  - Storage Systems:
    - GHI: filesystems/ghi.md

# Project Information
site_name: NERSC Development System Documentation
site_description: NERSC Development System Documentation
site_author: NERSC
site_dir: public
site_url: "https://docs-dev.nersc.gov/"
repo_name: GitLab/NERSC/docs-dev
repo_url: https://gitlab.com/NERSC/docs-dev
edit_uri: blob/master/docs/

# Configuration
strict: true

theme:
  name: material
  custom_dir: nersc-theme
  favicon: assets/images/favicon.ico
  logo: assets/images/logo.png
  features:
    - instant
  font:
    text: Open Sans

extra_javascript:
  - https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML
extra_css:
  - stylesheets/extra.css
    
extra:
  social:
    - icon: material/home-circle
      link: https://www.nersc.gov
    - icon: material/help-circle
      link: https://help.nersc.gov
    - icon: material/heart-pulse
      link: https://www.nersc.gov/live-status/motd/
    - icon: fontawesome/brands/github
      link: https://github.com/NERSC
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/NERSC
    - icon: fontawesome/brands/slack
      link: https://www.nersc.gov/users/NUG/nersc-users-slack/

# Extensions
markdown_extensions:
  - meta
  - attr_list
  - footnotes
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols:
      fractions: false
  - pymdownx.superfences
  - pymdownx.details
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - pymdownx.snippets

plugins:
  - search:
      prebuild_index: python
  - minify:
      minify_html: true
