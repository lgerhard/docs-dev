#----------------------------------------------------------#
#
#   Build stage #1 -- build layer
#
#----------------------------------------------------------#
FROM httpd:latest as builder

WORKDIR /tmp/build
USER root
ENV HOME /root
ENV SHELL /bin/bash
ENV BASH_ENV /etc/bash.bashrc
ENV DEBIAN_FRONTEND noninteractive
SHELL [ "/bin/bash", "--login", "-c" ]

# copy baseline code into container
COPY ./ /tmp/build/

# configure os
RUN apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get install -y git-core wget bzip2 libapache2-mod-vhost-ldap libapache2-mod-webauthldap && \
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh && \
    bash miniconda.sh -b -p /opt/conda && \
    export PATH=/opt/conda/bin:${PATH} && \
    conda config --set always_yes yes && \
    conda update -n base conda && \
    conda install -n base pip && \
    pip install -r requirements.txt && \
    mkdocs build && \
    cp -r public/* /usr/local/apache2/htdocs/ && \
    cp ./httpd-config/httpd.conf /usr/local/apache2/conf/httpd.conf && \
    cd /root && \
    apt-get -y autoclean && \
    rm -rf /tmp/build /var/lib/apt/lists/* /opt/conda

#----------------------------------------------------------#
#
#   Build stage #2 -- compress to two layers (httpd + custom)
#
#----------------------------------------------------------#
FROM httpd:latest
COPY --from=builder / /

USER root
ENV HOME /root
ENV SHELL /bin/bash
ENV BASH_ENV /etc/bash.bashrc
ENV DEBIAN_FRONTEND noninteractive
SHELL [ "/bin/bash", "--login", "-c" ]
# valuable for local debugging
EXPOSE 8080
